SUMMARY = "mlnx_cpldprog recipe"
DESCRIPTION = "Mellanox CPLD configuration utility"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${S}/COPYING;md5=94d55d512a9ba36caa9b7df079bae19f"

SRC_URI = "git://github.com/sholeksandr/mellanox-bmc-tools.git;branch=dev_jtag_class;protocol=https"
SRC_URI += "file://0001-Make-it-buildable-with-v29-kernel-code.patch"

SRCREV = "2c20cdf63983abd2be721d82b1a5fa430a5f36e3"
PR = "r0"
PV = "${SRCPV}"

S = "${WORKDIR}/git/mlnx_cpldprog"

EXTRA_OEMAKE = "KERNEL_SRC=${STAGING_KERNEL_DIR}"

do_install() {
    install -d ${D}${bindir}/
    install -m 0755 -D mlnx_cpldprog ${D}${bindir}/
}
